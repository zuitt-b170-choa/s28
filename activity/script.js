//test
console.log("Hello World");

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((json)=>console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((json)=>json.map(({title})=>(title)))
.then((json)=>console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=>response.json())
.then((json)=>console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=>response.json())
.then((json)=>console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		id: 1,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=>console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dataCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=>console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dataCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then((response)=> response.json())
.then((json)=>console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: true,
		status: "complete"
	})
})
.then((response)=> response.json())
.then((json)=>console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
